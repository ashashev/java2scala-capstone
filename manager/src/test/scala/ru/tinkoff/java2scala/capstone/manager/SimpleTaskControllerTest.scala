package ru.tinkoff.java2scala.capstone.manager

import java.util.concurrent.TimeoutException

import scala.concurrent.Await
import scala.concurrent.Future
import scala.concurrent.Promise
import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.Failure
import scala.util.Success
import scala.util.control.NoStackTrace

import akka.Done
import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.scaladsl.Flow
import akka.stream.scaladsl.Keep
import akka.stream.scaladsl.Sink
import akka.stream.scaladsl.Source
import akka.testkit.TestKit
import io.circe._
import io.circe.syntax._
import org.scalamock.scalatest.AsyncMockFactory
import org.scalatest.BeforeAndAfterAll
import org.scalatest.GivenWhenThen
import org.scalatest.OptionValues._
import org.scalatest.featurespec.AsyncFeatureSpecLike
import org.scalatest.matchers.should.Matchers

import ru.tinkoff.java2scala.capstone.commons.FieldName
import ru.tinkoff.java2scala.capstone.commons.TableName
import ru.tinkoff.java2scala.capstone.manager.TaskControlException.StopByUser
import ru.tinkoff.java2scala.capstone.manager.TaskControlException.TaskInterrupted
import ru.tinkoff.java2scala.capstone.manager.TaskControlException.TransferFailed
import ru.tinkoff.java2scala.capstone.transformation.FieldTransformation
import ru.tinkoff.java2scala.capstone.transformation.TableTransformation
import ru.tinkoff.java2scala.capstone.utils.FutureOps._

class SimpleTaskControllerTest
    extends TestKit(ActorSystem("TaskControllerTest"))
    with AsyncFeatureSpecLike
    with GivenWhenThen
    with BeforeAndAfterAll
    with Matchers
    with AsyncMockFactory {

  val readerMock = mockFunction[TableName, Source[Json, NotUsed]]
  val writerMock = mockFunction[TableName, Sink[Json, Future[Done]]]
  val fieldTransformationMock = mock[FieldTransformation]

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(actorSystem = system, verifySystemShutdown = true)
  }

  Feature("Task Control Exceptions has useful messages") {
    Scenario("StopByUser") {
      val e: Exception = new TaskControlException.StopByUser
      e.getMessage() shouldBe "StopByUser"
    }

    Scenario("TransferFailed") {
      val e: Exception = new TaskControlException.TransferFailed(TableName("TBL_1"), null)
      e.getMessage() shouldBe s"Transfer data from TBL_1 failed."
    }

    Scenario("TaskInterrupted") {
      val cause = new TaskControlException.TransferFailed(TableName("TBL_2"), null)
      val e: Exception = new TaskControlException.TaskInterrupted(TableName("TBL_1"), cause)
      e.getMessage() shouldBe s"Transfer data from TBL_1 was interrupted because transfer data from TBL_2 failed."
    }
  }

  def testSink(): (Future[Vector[Json]], Sink[Json, Future[Done]]) = {
    val p = Promise[Vector[Json]]
    val sink = Flow[Json]
      .fold(Vector.empty[Json])(_ :+ _)
      .map { xs =>
        p.success(xs)
        xs
      }
      .watchTermination() { (m, f) =>
        f.onComplete {
          case Failure(exception) => p.failure(exception)
          case Success(_) => ()
        }
        m
      }
      .toMat(Sink.ignore)(Keep.right)
    p.future -> sink
  }

  Feature("SimpleTaskController") {
    Scenario("Start transfer from single table") {
      val idGen = IdGenerator.constant("#1")
      val controller: SimpleTaskController = new SimpleTaskController(idGen)

      val data = Seq(JsonObject("F1" -> "payload".asJson).asJson)

      val (result, sink) = testSink()

      Given("table transformation rules")
      val table =
        TableTransformation(
          source = TableName("T1"),
          destination = TableName("T1N"),
          fields = Map(FieldName("F1") -> fieldTransformationMock)
        )

      readerMock.expects(table.source).returning(Source(data))
      writerMock.expects(table.destination).returning(sink)

      (fieldTransformationMock.apply _)
        .expects("payload".asJson)
        .returning("new payload".asJson.asSuccessful)

      When("controller create task")
      val taskId = controller.run(Vector(table), readerMock, writerMock)

      Then("should return id of created task")
      taskId shouldBe TaskId(idGen.gen())

      And("transformed data should come to sink")
      for {
        got <- result
      } yield {
        got should contain theSameElementsInOrderAs Seq(
          JsonObject("F1" -> "new payload".asJson).asJson
        )
      }
    }

    Scenario("Start transfer from two tables") {
      val idGen = IdGenerator.constant("#1")
      val controller: SimpleTaskController = new SimpleTaskController(idGen)

      Given("Run transfer from two non empty tables")
      val t1Data = Seq(
        JsonObject("F11" -> 110.asJson, "F12" -> "120".asJson).asJson,
        JsonObject("F11" -> 111.asJson, "F12" -> "121".asJson).asJson
      )
      val t1Source = Source(t1Data)
      val (t1Result, t1Sink) = testSink()
      (fieldTransformationMock.apply _).expects(110.asJson).returning(110.asJson.asSuccessful)
      (fieldTransformationMock.apply _).expects(111.asJson).returning(111.asJson.asSuccessful)
      (fieldTransformationMock.apply _).expects("120".asJson).returning("120".asJson.asSuccessful)
      (fieldTransformationMock.apply _).expects("121".asJson).returning("121".asJson.asSuccessful)

      val t2Data = Seq(
        JsonObject("F21" -> 210.asJson, "F22" -> "220".asJson).asJson,
        JsonObject("F21" -> 211.asJson, "F22" -> "221".asJson).asJson
      )
      val t2Source = Source(t2Data)
      val (t2Result, t2Sink) = testSink()
      (fieldTransformationMock.apply _).expects(210.asJson).returning(210.asJson.asSuccessful)
      (fieldTransformationMock.apply _).expects(211.asJson).returning(211.asJson.asSuccessful)
      (fieldTransformationMock.apply _).expects("220".asJson).returning("220".asJson.asSuccessful)
      (fieldTransformationMock.apply _).expects("221".asJson).returning("221".asJson.asSuccessful)

      val tables = Vector(
        TableTransformation(
          TableName("T1"),
          TableName("T1N"),
          Map(
            FieldName("F11") -> fieldTransformationMock,
            FieldName("F12") -> fieldTransformationMock
          )
        ),
        TableTransformation(
          TableName("T2"),
          TableName("T2N"),
          Map(
            FieldName("F21") -> fieldTransformationMock,
            FieldName("F22") -> fieldTransformationMock
          )
        )
      )

      readerMock.expects(TableName("T1")).returning(t1Source)
      writerMock.expects(TableName("T1N")).returning(t1Sink)

      readerMock.expects(TableName("T2")).returning(t2Source)
      writerMock.expects(TableName("T2N")).returning(t2Sink)

      val taskId = controller.run(tables, readerMock, writerMock)

      val task = controller.task(taskId).value

      When("Task finished")
      for {
        _ <- task.complete
        r1 <- t1Result
        r2 <- t2Result
      } yield {
        Then("Future for all tables should be complete")
        task.info.forall(_._2.isCompleted) shouldBe true

        And("Data in the sinks should be same the sources")
        r1 should contain theSameElementsInOrderAs t1Data
        r2 should contain theSameElementsInOrderAs t2Data
      }
    }

    Scenario("Stop task") {
      val idGen = IdGenerator.constant("#1")
      val controller: SimpleTaskController = new SimpleTaskController(idGen)

      val t1Promise = Promise[Json]
      val t1Source = Source.future(t1Promise.future)

      val t2Promise = Promise[Json]
      val t2Source = Source.future(t2Promise.future)

      val (_, t1Sink) = testSink()
      val (_, t2Sink) = testSink()

      val tables = Vector(
        TableTransformation(
          source = TableName("T1"),
          destination = TableName("T1"),
          fields = Map(FieldName("F1") -> fieldTransformationMock)
        ),
        TableTransformation(
          source = TableName("T2"),
          destination = TableName("T2"),
          fields = Map(FieldName("F1") -> fieldTransformationMock)
        )
      )

      readerMock.expects(TableName("T1")).returning(t1Source)
      readerMock.expects(TableName("T2")).returning(t2Source)

      writerMock.expects(TableName("T1")).returning(t1Sink)
      writerMock.expects(TableName("T2")).returning(t2Sink)

      Given("Run task")
      val taskId = controller.run(tables, readerMock, writerMock)
      val task = controller.task(taskId).value

      assertThrows[TimeoutException](Await.ready(task.complete, 1 second))

      When("call stop method")
      controller.stop(taskId) should not be empty

      Then("Task should finished with StopByUser exception")
      for {
        r <- task.complete.failed
        r1 <- task.info.head._2.failed
        r2 <- task.info(1)._2.failed
      } yield {
        r shouldBe a[StopByUser]
        r1 shouldBe a[StopByUser]
        r2 shouldBe a[StopByUser]
      }
    }

    Scenario("Task failed") {
      val idGen = IdGenerator.constant("#1")
      val controller: SimpleTaskController = new SimpleTaskController(idGen)

      val t1Promise = Promise[Json]
      val t1Source = Source.future(t1Promise.future)

      val t2Promise = Promise[Json]
      val t2Source = Source.future(t2Promise.future)

      val (_, t1Sink) = testSink()
      val (_, t2Sink) = testSink()

      val tables = Vector(
        TableTransformation(
          source = TableName("T1"),
          destination = TableName("T1"),
          fields = Map(FieldName("F1") -> fieldTransformationMock)
        ),
        TableTransformation(
          source = TableName("T2"),
          destination = TableName("T2"),
          fields = Map(FieldName("F1") -> fieldTransformationMock)
        )
      )

      readerMock.expects(TableName("T1")).returning(t1Source)
      readerMock.expects(TableName("T2")).returning(t2Source)

      writerMock.expects(TableName("T1")).returning(t1Sink)
      writerMock.expects(TableName("T2")).returning(t2Sink)

      Given("run task")
      val taskId = controller.run(tables, readerMock, writerMock)
      val task = controller.task(taskId).value

      assertThrows[TimeoutException](Await.ready(task.complete, 1 second))

      class TestException extends Exception with NoStackTrace

      When("transfer from one table failed")
      t2Promise.failure(new TestException)

      Then("task failed too with TransferFailed exception")
      for {
        r <- task.complete.failed
        r1 <- task.info.find(_._1 == TableName("T1")).value._2.failed
        r2 <- task.info.find(_._1 == TableName("T2")).value._2.failed
      } yield {
        r shouldBe a[TaskControlException]
        r1 shouldBe a[TaskInterrupted]
        r1.getCause() shouldBe a[TransferFailed]
        r2 shouldBe a[TransferFailed]
        r2.getCause() shouldBe a[TestException]
      }
    }

  }

}
