package ru.tinkoff.java2scala.capstone.manager

import scala.collection.concurrent.TrieMap
import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import scala.util.Failure

import akka.Done
import akka.NotUsed
import akka.stream.KillSwitches
import akka.stream.Materializer
import akka.stream.scaladsl.Sink
import akka.stream.scaladsl.Source
import com.typesafe.scalalogging.StrictLogging
import io.circe._

import ru.tinkoff.java2scala.capstone.commons.TableName
import ru.tinkoff.java2scala.capstone.manager.TaskControlException.StopByUser
import ru.tinkoff.java2scala.capstone.manager.TaskControlException.TransferFailed
import ru.tinkoff.java2scala.capstone.transformation.TableTransformation
import ru.tinkoff.java2scala.capstone.transformation.Transformation
import ru.tinkoff.java2scala.capstone.manager.TaskControlException.TaskInterrupted

class SimpleTaskController(
    idGen: IdGenerator
  )(implicit materializer: Materializer,
    ec: ExecutionContext)
    extends TaskController
    with StrictLogging {

  private val taskControls =
    TrieMap.empty[TaskId, TaskControl]

  override def run(
      tables: Vector[TableTransformation],
      reader: TableName => Source[Json, NotUsed],
      writer: TableName => Sink[Json, Future[Done]]
    ): TaskId = {
    val id = TaskId(idGen.gen())

    val kill = KillSwitches.shared(id.toString)

    val fs: Vector[(TableName, Future[Done])] = for {
      TableTransformation(src, dst, fields) <- tables
    } yield {
      src -> reader(src)
        .via(kill.flow)
        .via(Transformation.flow(fields))
        .runWith(writer(dst))
        .transform(
          identity, {
            case e: TransferFailed => new TaskInterrupted(src, e)
            case e: TaskControlException => e
            case e =>
              logger.error(s"Transfer $src -> $dst failed!", e)
              new TransferFailed(src, e)
          }
        )
        .andThen {
          case Failure(exception: TransferFailed) => kill.abort(exception)
          case _ => ()
        }
    }

    val f = Future.sequence(fs.map(_._2)).map(_ => Done)

    taskControls(id) = TaskControl(kill, TaskControlState(f, fs))
    id
  }

  override def tasks(): Seq[TaskId] = taskControls.keys.toSeq
  override def task(id: TaskId): Option[TaskControlState] = taskControls.get(id).map(_.state)
  override def stop(id: TaskId): Option[Unit] =
    taskControls.get(id).map(_.kill.abort(new StopByUser))
}
