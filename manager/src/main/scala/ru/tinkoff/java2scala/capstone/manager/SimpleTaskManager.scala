package ru.tinkoff.java2scala.capstone.manager

import scala.util.Try
import scala.concurrent.Future
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile
import com.typesafe.config.ConfigFactory
import ru.tinkoff.java2scala.capstone.transformation.FieldTransformationFactory
import ru.tinkoff.java2scala.capstone.transformation.TableTransformation
import ru.tinkoff.java2scala.capstone.db.DbReader
import ru.tinkoff.java2scala.capstone.db.DbWriter

class SimpleTaskManager(controller: TaskController, ftf: FieldTransformationFactory)
    extends TaskManager {

  override def run(task: Run): Future[TaskId] =
    Future.fromTry(Try {
      val srcDc =
        DatabaseConfig.forConfig[JdbcProfile]("", ConfigFactory.parseString(task.source.noSpaces))

      val dstDc = DatabaseConfig
        .forConfig[JdbcProfile]("", ConfigFactory.parseString(task.destination.noSpaces))

      val tables = task.scheme.map { s =>
        TableTransformation(
          s.tableFrom,
          s.tableTo,
          s.transformations.view.mapValues(ftf.apply).toMap
        )
      }

      controller.run(tables, DbReader.read(srcDc.profile, srcDc.db), DbWriter.write(dstDc.db))
    })

  override def tasks(): Future[Seq[TaskId]] = Future.fromTry(Try(controller.tasks()))

  override def task(id: String): Future[Option[TaskState]] =
    Future.fromTry(Try {
      controller.task(TaskId(id)).map(TaskState.from)
    })

  override def stop(id: String): Future[Option[Unit]] =
    Future.fromTry(Try(controller.stop(TaskId(id))))
}
