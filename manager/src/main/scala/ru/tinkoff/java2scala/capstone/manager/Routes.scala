package ru.tinkoff.java2scala.capstone.manager

import akka.http.scaladsl.server.Directives._
import de.heikoseeberger.akkahttpcirce.ErrorAccumulatingCirceSupport

trait Routes extends ErrorAccumulatingCirceSupport {
  val taskManager: TaskManager

  val routes = rejectEmptyResponse {
    path("run") {
      post(entity(as[Run])(task => complete(taskManager.run(task))))
    } ~ path("tasks") {
      get { complete(taskManager.tasks()) }
    } ~ path("task" / Segment) { id => get { complete(taskManager.task(id)) } }
  }
}
