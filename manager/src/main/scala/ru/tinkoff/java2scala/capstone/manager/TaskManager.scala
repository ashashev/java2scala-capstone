package ru.tinkoff.java2scala.capstone.manager

import scala.concurrent.Future

trait TaskManager {
  def run(task: Run): Future[TaskId]
  def tasks(): Future[Seq[TaskId]]
  def task(id: String): Future[Option[TaskState]]
  def stop(id: String): Future[Option[Unit]]
}
