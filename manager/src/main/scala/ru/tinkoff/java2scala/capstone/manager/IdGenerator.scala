package ru.tinkoff.java2scala.capstone.manager

import java.util.UUID

trait IdGenerator {
  def gen(): String
}

object IdGenerator {
  val random: IdGenerator = () => UUID.randomUUID.toString

  def constant(id: String): IdGenerator = () => id
}
