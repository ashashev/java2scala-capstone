package ru.tinkoff.java2scala.capstone.manager

import scala.concurrent.Future

import akka.Done
import akka.NotUsed
import akka.stream.scaladsl.Sink
import akka.stream.scaladsl.Source
import io.circe.Json

import ru.tinkoff.java2scala.capstone.commons.TableName
import ru.tinkoff.java2scala.capstone.transformation.TableTransformation

trait TaskController {
  def run(
      tables: Vector[TableTransformation],
      reader: TableName => Source[Json, NotUsed],
      writer: TableName => Sink[Json, Future[Done]]
    ): TaskId

  def tasks(): Seq[TaskId]
  def task(id: TaskId): Option[TaskControlState]
  def stop(id: TaskId): Option[Unit]
}
