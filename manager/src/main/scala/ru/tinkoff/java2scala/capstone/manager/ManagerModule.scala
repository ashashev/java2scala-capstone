package ru.tinkoff.java2scala.capstone.manager

import scala.annotation.nowarn

import com.softwaremill.macwire.Module
import com.softwaremill.macwire._

import ru.tinkoff.java2scala.capstone.commons.RuntimeModule
import ru.tinkoff.java2scala.capstone.transformation.TransformationModule

@Module @nowarn("msg=never used")
class ManagerModule(runtimeModule: RuntimeModule, transformationModule: TransformationModule) {
  self =>
  import runtimeModule._

  private val gen: IdGenerator = IdGenerator.random

  private val controller: TaskController = wire[SimpleTaskController]

  val taskManager: TaskManager = wire[SimpleTaskManager]

  private val managerRoutes: Routes = new Routes {
    override val taskManager: TaskManager = self.taskManager
  }

  val routes = managerRoutes.routes

}
