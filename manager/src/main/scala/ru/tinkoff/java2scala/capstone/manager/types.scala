package ru.tinkoff.java2scala.capstone.manager

import scala.concurrent.Future
import scala.util.Failure
import scala.util.Success
import scala.util.control.NoStackTrace

import akka.Done
import akka.stream.KillSwitch
import cats.Semigroup
import cats.syntax.option._
import cats.syntax.semigroup._
import enumeratum._
import io.circe._
import io.circe.generic.extras.semiauto.deriveUnwrappedCodec
import io.circe.generic.semiauto._

import ru.tinkoff.java2scala.capstone.commons.TableName
import ru.tinkoff.java2scala.capstone.transformation.Scheme

final case class Run(source: Json, destination: Json, scheme: Vector[Scheme])

object Run {
  implicit val codecRun: Codec[Run] = deriveCodec[Run]
}

sealed trait TaskStatus extends EnumEntry

object TaskStatus extends Enum[TaskStatus] with CirceEnum[TaskStatus] {
  val values = findValues

  case object Completed extends TaskStatus
  case object InProgress extends TaskStatus
  case object Failed extends TaskStatus

  implicit val semigroupTaskStatus: Semigroup[TaskStatus] = Semigroup.instance {
    case (Failed, _) => Failed
    case (_, Failed) => Failed
    case (InProgress, _) => InProgress
    case (_, InProgress) => InProgress
    case (Completed, Completed) => Completed
  }

}

final case class TaskInfo(table: TableName, status: TaskStatus, error: Option[String])

object TaskInfo {
  implicit val codecTaskInfo: Codec[TaskInfo] = deriveCodec[TaskInfo]

  def from(table: TableName, f: Future[Done]): TaskInfo = {
    val (status, error) = f.value match {
      case None => (TaskStatus.InProgress, None)
      case Some(Success(_)) => (TaskStatus.Completed, None)
      case Some(Failure(e)) =>
        (
          TaskStatus.Failed,
          (e.getMessage() + Option(e.getCause())
            .map(c => s" Cause: ${c.getMessage()}")
            .getOrElse("")).some
        )
    }

    TaskInfo(table, status, error)
  }
}

final case class TaskState(status: TaskStatus, info: Vector[TaskInfo])

object TaskState {
  implicit val codecTaskState: Codec[TaskState] = deriveCodec[TaskState]

  def from(v: TaskControlState): TaskState = {
    val info = v.info.map { case (tbl, f) => TaskInfo.from(tbl, f) }
    val status = info.map(_.status).fold(TaskStatus.Completed)(_ |+| _)
    TaskState(status, info)
  }

}

final class TaskId(val get: String) extends AnyVal {
  override def toString(): String = get
}

object TaskId {
  def apply(v: String): TaskId = new TaskId(v)

  implicit val keyEncodeTaskId: KeyEncoder[TaskId] =
    KeyEncoder.encodeKeyString.contramap(_.get)

  implicit val keyDecodeTaskId: KeyDecoder[TaskId] =
    KeyDecoder.decodeKeyString.map(TaskId.apply)

  implicit val codecRunResult: Codec[TaskId] = deriveUnwrappedCodec[TaskId]
}

sealed class TaskControlException(msg: String, cause: Throwable)
    extends RuntimeException(msg, cause)
    with NoStackTrace {
  def this(msg: String) = this(msg, null)
}

object TaskControlException {

  final class StopByUser extends TaskControlException(classOf[StopByUser].getSimpleName())

  final class TransferFailed(val table: TableName, cause: Throwable)
      extends TaskControlException(s"Transfer data from $table failed.", cause)

  final class TaskInterrupted(val table: TableName, cause: TransferFailed)
      extends TaskControlException(
        s"Transfer data from $table was interrupted because transfer data from ${cause.table} failed.",
        cause
      )
}

final case class TaskControlState(complete: Future[Done], info: Vector[(TableName, Future[Done])])

final case class TaskControl(kill: KillSwitch, state: TaskControlState)
