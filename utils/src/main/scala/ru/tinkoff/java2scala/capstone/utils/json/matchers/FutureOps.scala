package ru.tinkoff.java2scala.capstone.utils

import scala.concurrent.Future

object FutureOps {
  implicit class FutureSyntax[T](val v: T) extends AnyVal {
    def asSuccessful: Future[T] = Future.successful(v)
  }

}
