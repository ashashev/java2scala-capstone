package ru.tinkoff.java2scala.capstone.utils.json.matchers

import io.circe._

object IsObject {
  def unapply(v: Json): Option[JsonObject] = v.asObject
}

object IsNull {
  def unapply(v: Json): Option[Unit] = v.asNull
}

object IsString {
  def unapply(v: Json): Option[String] = v.asString
}

object IsLong {
  def unapply(v: Json): Option[Long] = v.asNumber.flatMap(_.toLong)
}

object IsBigDecimal {
  def unapply(v: Json): Option[BigDecimal] = v.asNumber.flatMap(_.toBigDecimal)
}

object IsBoolean {
  def unapply(v: Json): Option[Boolean] = v.asBoolean
}
