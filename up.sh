#!/bin/bash

rm capstone-vol/capstone.log

docker-compose -f test-env.yaml --compatibility up -d

dst_db=$(docker ps --format {{.Names}} | fgrep 'dst-db');
src_db=$(docker ps --format {{.Names}} | fgrep 'src-db');
local_db=$(docker ps --format {{.Names}} | fgrep 'local-db');


function waiting_for_postgres {
    echo "waiting for $1..."
    until docker exec $1 pg_isready -U user -d mydb; do
        echo "wating for $1..."
        sleep 1
    done
    echo "$1 - READY"
}

waiting_for_postgres $src_db
docker exec -i $src_db psql -U user -d mydb < test_tables.sql
docker exec -i $src_db psql -U user -d mydb < test_tables_fill.sql

waiting_for_postgres $dst_db
docker exec -i $dst_db psql -U user -d mydb < test_tables.sql

waiting_for_postgres $local_db
docker exec -i $local_db psql -U user -d mydb < transformation/src/main/resources/transformation_cache_table.sql
