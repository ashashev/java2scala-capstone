import sbt._

object Dependencies {
  val scalaTest = "org.scalatest" %% "scalatest" % "3.1.1"
  val scalaTestPlusScalaCheck = "org.scalatestplus" %% "scalacheck-1-14" % "3.1.0.0"
  val scalaMock = "org.scalamock" %% "scalamock" % "4.4.0"
  val scalaCheck = "org.scalacheck" %% "scalacheck" % "1.14.1"

  val macwire = Seq(
    "com.softwaremill.macwire" %% "macros" % "2.3.3" % Provided,
    "com.softwaremill.macwire" %% "util" % "2.3.3"
  )

  val cats = Seq(
    "org.typelevel" %% "cats-core" % "2.0.0"
  )

  val slick = "com.typesafe.slick" %% "slick" % "3.3.2"
  val slickHikaricp = "com.typesafe.slick" %% "slick-hikaricp" % "3.3.2"
  val slickH2db = "com.h2database" % "h2" % "1.4.200"
  val postgres = "org.postgresql" % "postgresql" % "42.2.5"

  val akkaVersion = "2.6.4"
  val akkaStream = Seq(
    "com.typesafe.akka" %% "akka-stream" % akkaVersion,
    "com.typesafe.akka" %% "akka-stream-typed" % akkaVersion,
    "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
    "com.typesafe.akka" %% "akka-testkit" % akkaVersion % Test,
    "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion % Test
  )

  val akkaHttpVersion = "10.1.11"
  val akkaHttp = Seq(
    "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
    "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion,
    "de.heikoseeberger" %% "akka-http-circe" % "1.32.0"
  )

  val enumeratum = "com.beachape" %% "enumeratum" % "1.5.15"
  val enumeratumCirce = "com.beachape" %% "enumeratum-circe" % "1.5.23"

  val circeVersion = "0.13.0"
  val circe = Seq(
    "circe-core",
    "circe-generic",
    "circe-generic-extras",
    "circe-parser"
  ).map("io.circe" %% _ % circeVersion)

  val scalaLogging = Seq(
    "ch.qos.logback" % "logback-classic" % "1.2.3",
    "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2"
  )
}
