package ru.tinkoff.java2scala.capstone.commons

import io.circe._
import io.circe.generic.extras.semiauto._

final class TableName(val get: String) extends AnyVal {
  override def toString(): String = get
}

object TableName {
  def apply(v: String): TableName = new TableName(v)

  implicit val keyEncodeFieldName: KeyEncoder[TableName] =
    KeyEncoder.encodeKeyString.contramap(_.get)
  implicit val keyDecodeFieldName: KeyDecoder[TableName] =
    KeyDecoder.decodeKeyString.map(TableName.apply)

  implicit val codecTableName: Codec[TableName] = deriveUnwrappedCodec
}

final class FieldName(val get: String) extends AnyVal {
  override def toString(): String = get
}

object FieldName {
  def apply(v: String): FieldName = new FieldName(v)

  implicit val keyEncodeFieldName: KeyEncoder[FieldName] =
    KeyEncoder.encodeKeyString.contramap(_.get)
  implicit val keyDecodeFieldName: KeyDecoder[FieldName] =
    KeyDecoder.decodeKeyString.map(FieldName.apply)

  implicit val codecFieldName: Codec[FieldName] = deriveUnwrappedCodec
}
