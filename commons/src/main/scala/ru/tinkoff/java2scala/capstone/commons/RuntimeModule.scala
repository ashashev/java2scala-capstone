package ru.tinkoff.java2scala.capstone.commons

import scala.concurrent.ExecutionContext

import akka.actor.ActorSystem
import slick.jdbc.JdbcProfile

trait RuntimeModule {
  implicit val actorSystem: ActorSystem
  implicit val executionContext: ExecutionContext
  val dbProfile: JdbcProfile
  val db: JdbcProfile#API#Database
}
