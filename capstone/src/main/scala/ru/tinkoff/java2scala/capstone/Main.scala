package ru.tinkoff.java2scala.capstone

import scala.concurrent.ExecutionContext
import scala.util.Failure
import scala.util.Success

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import com.softwaremill.macwire._
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.StrictLogging
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcBackend
import slick.jdbc.JdbcProfile

import ru.tinkoff.java2scala.capstone.commons.RuntimeModule
import ru.tinkoff.java2scala.capstone.manager.ManagerModule
import ru.tinkoff.java2scala.capstone.transformation.TransformationModule
import ru.tinkoff.java2scala.capstone.cachecontrol.CacheControlModule

object Main extends App with StrictLogging {

  val capstoneConfig = ConfigFactory.load().getConfig("capstone")
  val dbc = DatabaseConfig.forConfig[JdbcProfile]("database", capstoneConfig)

  implicit val system: ActorSystem = ActorSystem("java2scala-capstone")

  system.registerOnTermination(dbc.db.close())

  val runtimeModule: RuntimeModule = new RuntimeModule {
    override val actorSystem: ActorSystem = system

    override val executionContext: ExecutionContext = system.dispatcher

    override val dbProfile: JdbcProfile = dbc.profile

    override val db: JdbcBackend#DatabaseDef = dbc.db
  }

  val transformationModule: TransformationModule = wire[TransformationModule]

  val cacheControlModule: CacheControlModule = wire[CacheControlModule]

  val managerModule: ManagerModule = wire[ManagerModule]

  val routes =
    get {
      path("check")(complete("OK"))
    } ~ managerModule.routes ~
    pathPrefix("cache") {
      cacheControlModule.routes
    }

  val bindingFuture = Http().bindAndHandle(routes, "0.0.0.0", 3435)
  bindingFuture.onComplete {
    case Success(binding) =>
      logger.info(s"Server listen ${binding.localAddress}.")
    case Failure(e) =>
      logger.error("Can't start server!", e)
      system.terminate()
  }(system.dispatcher)

}
