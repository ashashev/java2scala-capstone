package ru.tinkoff.java2scala.capstone.cachecontrol

import scala.annotation.nowarn

import com.softwaremill.macwire.Module

import ru.tinkoff.java2scala.capstone.commons.RuntimeModule
import ru.tinkoff.java2scala.capstone.transformation.Cache
import scala.concurrent.ExecutionContext

@Module @nowarn("msg=never used")
class CacheControlModule(runtimeModule: RuntimeModule, cache: Cache) { self =>

  private val cacheControlRoutes: Routes = new Routes {
    override val ec: ExecutionContext = runtimeModule.executionContext
    override val cache: Cache = self.cache
  }

  val routes = cacheControlRoutes.routes
}
