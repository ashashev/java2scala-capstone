package ru.tinkoff.java2scala.capstone.cachecontrol

import io.circe._
import io.circe.generic.semiauto._

final case class Replacement(value: Json, replace: Json)

object Replacement {
  implicit val codecReplacement: Codec[Replacement] = deriveCodec[Replacement]
}