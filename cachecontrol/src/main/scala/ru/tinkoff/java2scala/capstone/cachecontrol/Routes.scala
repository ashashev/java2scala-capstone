package ru.tinkoff.java2scala.capstone.cachecontrol

import scala.concurrent.ExecutionContext

import akka.http.scaladsl.server.Directives._
import de.heikoseeberger.akkahttpcirce.ErrorAccumulatingCirceSupport

import ru.tinkoff.java2scala.capstone.transformation.Cache
import ru.tinkoff.java2scala.capstone.transformation.DataType
import com.typesafe.scalalogging.StrictLogging

trait Routes extends ErrorAccumulatingCirceSupport with StrictLogging {

  implicit val ec: ExecutionContext
  val cache: Cache

  private val dataTypeMatcher = DataType.values.map(dt => (dt.entryName -> dt)).toMap

  val routes = rejectEmptyResponse {
    path("types") {
      get { complete(DataType.values) }
    } ~ path("replacements" / dataTypeMatcher) { dt =>
      get { complete(cache.getAll(dt).map(_.map { case (v, r) => Replacement(v, r) })) } ~
        post {
          entity(as[List[Replacement]]) { vrs =>
            complete(cache.putAll(dt, vrs.map(r => r.value -> r.replace)).map(_ => "OK"))
          }
        }
    }
  }
}
