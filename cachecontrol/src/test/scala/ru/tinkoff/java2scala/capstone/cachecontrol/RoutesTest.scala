package ru.tinkoff.java2scala.capstone.cachecontrol

import scala.annotation.nowarn
import scala.concurrent.ExecutionContext
import scala.concurrent.Future

import akka.Done
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.ScalatestRouteTest
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe._
import io.circe.syntax._
import org.scalamock.scalatest.MockFactory
import org.scalatest.AppendedClues
import org.scalatest.BeforeAndAfterAll
import org.scalatest.EitherValues._
import org.scalatest.OptionValues._
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

import ru.tinkoff.java2scala.capstone.transformation.Cache
import ru.tinkoff.java2scala.capstone.transformation.DataType

@nowarn("msg=method right in class Either is deprecated")
class RoutesTest
    extends AnyFunSuite
    with AppendedClues
    with Matchers
    with BeforeAndAfterAll
    with ScalatestRouteTest
    with FailFastCirceSupport
    with MockFactory {

  val cacheMock = mock[Cache]

  val cacheControlRoutes = new Routes {
    override val ec: ExecutionContext = system.dispatcher

    override val cache: Cache = cacheMock
  }

  val routes = cacheControlRoutes.routes

  test("returns all data types") {
    Get("/types") ~> routes ~> check {
      status shouldBe StatusCodes.OK
      val body = responseAs[Json]
      body.asArray.value should contain theSameElementsAs DataType.values.map(_.entryName.asJson)
    }
  }

  test("returns replacements") {

    val expects = parser
      .parse("""[
               |  {"value": 10.0, "replace": 100.0},
               |  {"value": 13.0, "replace": 42.0}
               |]""".stripMargin)
      .right
      .value

    val cachedData = Seq(10.0.asJson -> 100.0.asJson, 13.0.asJson -> 42.0.asJson)
    (cacheMock.getAll _).expects(DataType.Money).returning(Future.successful(cachedData))

    Get(s"/replacements/${DataType.Money.entryName}") ~> routes ~> check {
      status shouldBe StatusCodes.OK
      responseAs[Json] shouldBe expects
    }
  }

  test("insert replacements") {
    val input = parser.parse("""[
                  |  {"value": 10.0, "replace": 100.0},
                  |  {"value": 13.0, "replace": 42.0}
                  |]""".stripMargin).right.value

    val expects = Seq(10.0.asJson -> 100.0.asJson, 13.0.asJson -> 42.0.asJson)
    (cacheMock.putAll _).expects(DataType.Money, expects).returning(Future.successful(Done))

    Post(s"/replacements/${DataType.Money.entryName}", input) ~> routes ~> check {
      (status shouldBe StatusCodes.OK) withClue (responseAs[String])
      responseAs[String] shouldBe "OK"
    }
  }
}
