#!/bin/bash

package_folders="scala/ru/tinkoff/java2scala/capstone"

if [[ "$1" == "" ]]; then
  echo "Not set name"
  exit 1
fi

mkdir -p "$1/src/main/$package_folders/$1"
mkdir -p "$1/src/main/resources"
mkdir -p "$1/src/test/$package_folders/$1"
mkdir -p "$1/src/test/resources"


