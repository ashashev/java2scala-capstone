package ru.tinkoff.java2scala.capstone.db

import akka.NotUsed
import akka.stream.scaladsl.Source
import io.circe.Json
import slick.jdbc.JdbcProfile

import ru.tinkoff.java2scala.capstone.db.util.ResultAsJson._
import ru.tinkoff.java2scala.capstone.commons.TableName

object DbReader {
  def read(
      profile: JdbcProfile,
      db: JdbcProfile#API#Database
    )(tableName: TableName
    ): Source[Json, NotUsed] = {
    import profile.api._
    Source.fromPublisher(db.stream(sql"SELECT * FROM #${tableName.get}".as[Json]))
  }
}
