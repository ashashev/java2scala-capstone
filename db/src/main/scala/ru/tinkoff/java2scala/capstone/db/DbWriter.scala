package ru.tinkoff.java2scala.capstone.db

import scala.concurrent.Future

import akka.Done
import akka.stream.scaladsl.Flow
import akka.stream.scaladsl.Keep
import akka.stream.scaladsl.Sink
import io.circe.Json
import slick.jdbc.JdbcProfile
import slick.jdbc.PositionedParameters
import slick.jdbc.SQLActionBuilder

import ru.tinkoff.java2scala.capstone.commons.TableName
import ru.tinkoff.java2scala.capstone.utils.json.matchers._

object DbWriter {
  def write(db: JdbcProfile#API#Database)(tableName: TableName): Sink[Json, Future[Done]] = {
    val q = insert(tableName.get)(_)
    Flow[Json].mapAsync(1)(j => db.run(q(j).asUpdate)).toMat(Sink.ignore)(Keep.right)
  }

  def insert(tableName: String)(json: Json) = json match {
    case IsObject(o) =>
      val (fields, values) = o.toList.unzip
      val query =
        s"""INSERT INTO $tableName (${fields.mkString(", ")})
           |  VALUES (${fields.map(_ => "?").mkString(", ")});""".stripMargin

      new SQLActionBuilder(Seq(query), (_: Unit, pp: PositionedParameters) => {
        set(pp, values)
        ()
      })
    case _ => throw new RuntimeException(s"Expect JSON object, but got: ${json.noSpaces}")
  }

  def set(pp: PositionedParameters, vs: List[Json]): PositionedParameters = vs match {
    case Nil => pp
    case v :: rs =>
      v match {
        case IsNull(_) =>
          pp.setNull(pp.ps.getParameterMetaData().getParameterType(pp.pos + 1))
          set(pp, rs)
        case IsString(value) =>
          pp.setString(value)
          set(pp, rs)
        case IsBoolean(value) =>
          pp.setBoolean(value)
          set(pp, rs)
        case IsLong(value) =>
          pp.setLong(value)
          set(pp, rs)
        case IsBigDecimal(value) =>
          pp.setBigDecimal(value)
          set(pp, rs)
      }
  }
}
