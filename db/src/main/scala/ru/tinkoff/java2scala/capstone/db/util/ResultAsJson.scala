package ru.tinkoff.java2scala.capstone.db.util

import java.sql.ResultSet
import java.sql.ResultSetMetaData
import java.sql.Types

import io.circe._
import io.circe.syntax._
import slick.jdbc.GetResult
import slick.jdbc.PositionedResult

object ResultAsJson {
  implicit val jsonResult: GetResult[Json] = new GetResult[Json] {

    override def apply(pr: PositionedResult): Json = {
      val md = pr.rs.getMetaData();
      val getJson = getJsonFromResultSet(pr.rs, md)(_)

      val res = (for {
        i <- (1 to pr.numColumns)
        name = md.getColumnName(i)
        j = getJson(i)
      } yield (name -> j))

      JsonObject(res: _*).asJson
    }

  }

  private def getJsonFromResultSet(rs: ResultSet, md: ResultSetMetaData)(num: Int): Json = {
    if (rs.getObject(num) == null) Json.Null
    else
      md.getColumnType(num) match {
        case Types.NULL => Json.Null
        case Types.VARCHAR => rs.getString(num).asJson
        case Types.INTEGER => rs.getInt(num).asJson
        case Types.DECIMAL => rs.getBigDecimal(num).asJson
        case Types.DATE => rs.getDate(num).toLocalDate.asJson
        case Types.TIME => rs.getTime(num).toLocalTime.asJson
        case Types.BOOLEAN => rs.getBoolean(num).asJson
        case _ if md.getColumnTypeName(num) == "bool" => rs.getBoolean(num).asJson
        case t =>
          throw new RuntimeException(
            s"Unknown SQL type: ${md.getColumnTypeName(num)} ($t), value: ${rs.getObject(num)}"
          )
      }
  }
}
