package ru.tinkoff.java2scala.capstone.db.util

import slick.jdbc.GetResult
import slick.jdbc.PositionedResult

object ResultAsMap {

  implicit val getResultAsMap = new GetResult[Map[String, Any]] {
    override def apply(pr: PositionedResult): Map[String, Any] = {
      val rs = pr.rs
      val md = rs.getMetaData();
      val res = (1 to pr.numColumns).map { i => md.getColumnName(i) -> rs.getObject(i) }.toMap
      res
    }
  }

}
