package ru.tinkoff.java2scala.capstone.db.util

import scala.annotation.nowarn

import io.circe._
import org.scalactic.source.Position
import org.scalatest.EitherValues._

object Extenders {
  implicit class StringTestExetenders(val s: String) extends AnyVal {

    @nowarn("msg=method right in class Either is deprecated")
    def parseAsJson(implicit pos: Position): Json =
      parser.parse(s).right.value

  }
}
