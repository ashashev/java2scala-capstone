package ru.tinkoff.java2scala.capstone.db.util

import io.circe._
import org.scalatest.funsuite.AsyncFunSuite
import org.scalatest.matchers.should.Matchers
import slick.jdbc.H2Profile.api._

import ru.tinkoff.java2scala.capstone.db.util.Extenders._

class JsonResultTest extends AsyncFunSuite with Matchers {

  val db = Database.forURL("jdbc:h2:mem:test1;DB_CLOSE_DELAY=-1", driver = "org.h2.Driver")

  val tableName = "TEST_DATA"
  val tableScheme =
    sqlu"""CREATE TABLE #$tableName (
      F0 VARCHAR,
      F1 INTEGER,
      F2 DECIMAL,
      F3 DATE,
      F4 TIME,
      F5 BOOLEAN
    );"""

  val examples = DBIO.seq(
    sqlu"INSERT INTO TEST_DATA(F0, F1, F2, F3, F4, F5) VALUES(null, null, null, null, null, null);",
    sqlu"INSERT INTO TEST_DATA(F0, F1, F2, F3, F4, F5) VALUES(${"test1"}, ${42}, ${123.43}, ${"2010-10-31"}, ${"13:23:42"}, ${false});",
    sqlu"INSERT INTO TEST_DATA(F0, F1, F2, F3, F4, F5) VALUES('test2', 1000, 50.0, '2020-03-31', '9:08:05', true);"
  )

  val dbReady = db.run(
    for {
      _ <- tableScheme
      _ <- examples
    } yield ()
  )

  import ResultAsJson._

  test("Read row as JSON")(dbReady.flatMap { _ =>
    val expects = Vector(
      """{"F0": null, "F1": null, "F2": null, "F3": null, "F4": null, "F5": null}""".parseAsJson,
      """{"F0": "test1", "F1": 42, "F2": 123.43, "F3": "2010-10-31", "F4": "13:23:42", "F5": false}""".parseAsJson,
      """{"F0": "test2", "F1": 1000, "F2": 50.0, "F3": "2020-03-31", "F4": "09:08:05", "F5": true}""".parseAsJson
    )

    db.run(sql"SELECT * FROM TEST_DATA".as[Json]).map { js =>
      for {
        i <- 0 until js.size
        got: Json = js(i)
        expect: Json = expects(i)
      } yield {
        got.toString shouldBe expect.toString
      }

      succeed
    }
  })
}
