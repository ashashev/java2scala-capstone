package ru.tinkoff.java2scala.capstone.db

import scala.concurrent.Future

import akka.Done
import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.scaladsl.Sink
import akka.stream.scaladsl.Source
import akka.testkit.TestKit
import io.circe._
import org.scalatest.BeforeAndAfter
import org.scalatest.BeforeAndAfterAll
import org.scalatest.funsuite.AsyncFunSuiteLike
import org.scalatest.matchers.should.Matchers
import slick.jdbc.H2Profile.api._

import ru.tinkoff.java2scala.capstone.db.util.Extenders._
import ru.tinkoff.java2scala.capstone.commons.TableName

class DbReaderTest
    extends TestKit(ActorSystem("DbReaderTest"))
    with AsyncFunSuiteLike
    with BeforeAndAfter
    with BeforeAndAfterAll
    with Matchers {

  var db: Database = _

  val coffees = TableQuery[Coffees]

  def dbReady(data: Seq[(String, Int, BigDecimal, Int, Int, Boolean)]) =
    db.run(for {
      _ <- coffees.schema.createIfNotExists
      _ <- coffees ++= data
    } yield ())

  before {
    db = Database.forURL(
      s"jdbc:h2:mem:${java.util.UUID.randomUUID.toString()};DB_CLOSE_DELAY=-1",
      driver = "org.h2.Driver"
    )
  }

  after {
    db.close()
  }

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(actorSystem = system, verifySystemShutdown = true)
  }

  test("DbReader make stream with all data")(dbReady(Coffees.example).flatMap { _ =>
    val source = DbReader.read(slick.jdbc.H2Profile, db)(TableName("COFFEES"))
    val result = source.runWith(Sink.seq)

    val expect = Seq(
      """{"COF_NAME": "espresso", "SUP_ID": 100, "PRICE": 90.0, "SALES": 0, "TOTAL": 0, "WITH_MILK": false}""".parseAsJson,
      """{"COF_NAME": "americano", "SUP_ID": 101, "PRICE": 90.0, "SALES": 0, "TOTAL": 0, "WITH_MILK": false}""".parseAsJson,
      """{"COF_NAME": "cappuchino", "SUP_ID": 102, "PRICE": 130.0, "SALES": 0, "TOTAL": 0, "WITH_MILK": true}""".parseAsJson
    )

    result.map(_ should contain theSameElementsInOrderAs expect)
  })

  test("DbWriter writes all incomming data to DB") {
    for {
      _ <- dbReady(Seq.empty)
      initial <- db.run(coffees.result)
      source: Source[Json, NotUsed] = Source(
        Seq(
          """{"COF_NAME": "espresso", "SUP_ID": 100, "PRICE": 90.0, "SALES": 0, "TOTAL": 0, "WITH_MILK": false}""".parseAsJson,
          """{"COF_NAME": "americano", "SUP_ID": 101, "PRICE": 90.0, "SALES": 0, "TOTAL": 0, "WITH_MILK": false}""".parseAsJson,
          """{"COF_NAME": "cappuchino", "SUP_ID": 102, "PRICE": 130.0, "SALES": 0, "TOTAL": 0, "WITH_MILK": true}""".parseAsJson
        )
      )
      sink: Sink[Json, Future[Done]] = DbWriter.write(db)(TableName("COFFEES"))
      _ <- source.runWith(sink)
      end <- db.run(coffees.result)
    } yield {
      initial shouldBe empty
      end should contain theSameElementsAs Coffees.example
    }
  }

}

class Coffees(tag: Tag)
    extends Table[(String, Int, BigDecimal, Int, Int, Boolean)](tag, "COFFEES") {
  def name = column[String]("COF_NAME", O.PrimaryKey)
  def supID = column[Int]("SUP_ID")
  def price = column[BigDecimal]("PRICE")
  def sales = column[Int]("SALES", O.Default(0))
  def total = column[Int]("TOTAL", O.Default(0))
  def withMilk = column[Boolean]("WITH_MILK")
  def * = (name, supID, price, sales, total, withMilk)
}

object Coffees {
  val columnNames = Seq("COF_NAME", "SUP_ID", "PRICE", "SALES", "TOTAL", "WITH_MILK")

  val example = Seq(
    ("espresso", 100, BigDecimal(90.0), 0, 0, false),
    ("americano", 101, BigDecimal(90.0), 0, 0, false),
    ("cappuchino", 102, BigDecimal(130.0), 0, 0, true)
  )
}
