package ru.tinkoff.java2scala.capstone.transformation

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers
import cats.syntax.either._
import io.circe.syntax._
import io.circe._

import ru.tinkoff.java2scala.capstone.commons.FieldName
import ru.tinkoff.java2scala.capstone.commons.TableName

class FieldSchemeTransformationTest extends AnyFunSuite with Matchers {

  test("Encode TableName") {
    val t = TableName("MY_TABLE")
    val expect: Json = "MY_TABLE".asJson
    t.asJson shouldBe expect
  }

  test("Decode TableName") {
    val input: Json = "MY_TABLE".asJson
    input.as[TableName] shouldBe TableName("MY_TABLE").asRight[DecodingFailure]
  }

  test("Encode FieldName") {
    val t = FieldName("LASTNAME")
    val expect: Json = "LASTNAME".asJson
    t.asJson shouldBe expect
  }

  test("Decode FieldName") {
    val input: Json = "LASTNAME".asJson
    input.as[FieldName] shouldBe FieldName("LASTNAME").asRight[DecodingFailure]
  }

  test("Encode IdentityScheme") {
    val t: FieldSchemeTransformation = IdentityScheme
    val expect: Json = "IDENTITY".asJson
    t.asJson shouldBe expect
  }

  test("Decode IdentityScheme") {
    val input: Json = "IDENTITY".asJson
    input.as[FieldSchemeTransformation] shouldBe IdentityScheme.asRight[DecodingFailure]
  }

}
