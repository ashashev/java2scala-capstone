package ru.tinkoff.java2scala.capstone.transformation

import io.circe._
import org.scalatest.Inspectors
import org.scalatest.OptionValues._
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

class DataGeneratorTest extends AnyFunSuite with Matchers with Inspectors {
  val probeCount = 100

  val generators: DataGeneratorFactory = new SimpleDataGeneratorFactory

  def probes(dt: DataType): Seq[Json] = {
    val g = generators(dt)
    (0 to probeCount).map(_ => g.gen())
  }

  test("Address") {
    val ps = probes(DataType.Address)
    forAll(ps) { probe =>
      probe.asString.value should fullyMatch regex "[A-Z][a-z]+, st. [A-Z][a-z]+, [0-9]+, [0-9]+"
    }
  }

  test("BankAccount") {
    val ps = probes(DataType.BankAccount)
    forAll(ps) { probe =>
      probe.asString.value should fullyMatch regex "[0-9]{10,10}-[A-Z]{4,4}-[0-9]{3,3}"
    }
  }

  test("Date") {
    val ps = probes(DataType.Date)
    forAll(ps) { probe =>
      probe.asString.value should fullyMatch regex "[1,2][0-9]{3,3}-[0,1][0-9]-[0-3][0-9]"
    }
  }

  test("Email") {
    val ps = probes(DataType.Email)
    forAll(ps) { probe =>
      probe.asString.value should fullyMatch regex raw"[a-z0-9]{2,20}@[a-z0-9]{2,10}\.[a-z]{2,5}"
    }
  }

  test("FirstName") {
    val ps = probes(DataType.FirstName)
    forAll(ps) { probe => probe.asString.value should fullyMatch regex "[A-Z][a-z]{1,30}" }
  }

  test("LastName") {
    val ps = probes(DataType.LastName)
    forAll(ps) { probe => probe.asString.value should fullyMatch regex "[A-Z][a-z]{1,30}" }
  }

  test("Money") {
    val ps = probes(DataType.Money)
    forAll(ps) { probe =>
      probe.asNumber.value.toString should fullyMatch regex raw"([0-9]*){0,1}(\.[0-9]{1,2}){0,1}"
    }
  }

  test("Phone") {
    val ps = probes(DataType.Phone)
    forAll(ps) { probe => probe.asString.value should fullyMatch regex raw"\+7[0-9]{10,10}" }
  }
}
