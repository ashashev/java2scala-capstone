package ru.tinkoff.java2scala.capstone.transformation

import akka.actor.ActorSystem
import akka.stream.scaladsl.Sink
import akka.stream.scaladsl.Source
import akka.testkit.TestKit
import org.scalatest.funsuite.AsyncFunSuiteLike
import org.scalatest.matchers.should.Matchers
import io.circe.syntax._
import io.circe._

import ru.tinkoff.java2scala.capstone.commons.FieldName
import ru.tinkoff.java2scala.capstone.transformation.Transformation.NoFieldTransformation

class TransformationTest
    extends TestKit(ActorSystem("TransformationTest"))
    with AsyncFunSuiteLike
    with Matchers {

  test("Transformation flow with Identity Transformation doesn't modify stream") {
    val js = Seq(
      JsonObject("f1" -> 12.asJson, "f2" -> "foo".asJson).asJson,
      JsonObject("f1" -> 43.asJson, "f2" -> "bar".asJson).asJson,
      JsonObject("f1" -> 76.asJson, "f2" -> "qoo".asJson).asJson
    )

    val in = Source(js)
    val out = Sink.seq[Json]

    in.via(
        Transformation.flow(
          Map(FieldName("f1") -> IdentityTransformation, FieldName("f2") -> IdentityTransformation)
        )
      )
      .runWith(out)
      .map(_ should contain theSameElementsInOrderAs js)

  }

  test("Transformation flow should throw exception if transformation doesn't contain field") {
    val js = Seq(
      JsonObject("f1" -> 12.asJson, "f2" -> "foo".asJson).asJson
    )

    val in = Source(js)
    val out = Sink.seq[Json]

    in.via(
        Transformation.flow(
          Map(FieldName("f1") -> IdentityTransformation)
        )
      )
      .runWith(out)
      .failed
      .map(_ shouldBe a[NoFieldTransformation])

  }

}
