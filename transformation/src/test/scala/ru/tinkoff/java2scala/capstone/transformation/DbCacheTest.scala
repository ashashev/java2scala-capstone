package ru.tinkoff.java2scala.capstone.transformation

//import scala.concurrent.Future
import scala.util.Using

import io.circe._
import io.circe.syntax._
import org.scalatest.BeforeAndAfter
import org.scalatest.BeforeAndAfterAll
import org.scalatest.OptionValues._
import org.scalatest.funsuite.AsyncFunSuite
import org.scalatest.matchers.should.Matchers
import slick.jdbc.JdbcProfile

class DbCacheTest extends AsyncFunSuite with BeforeAndAfter with BeforeAndAfterAll with Matchers {

  val createTransformationCacheSql =
    Using(scala.io.Source.fromResource("transformation_cache_table.sql")) { bs =>
      bs.getLines().mkString("\n")
    }.get

  val profile: JdbcProfile = slick.jdbc.H2Profile

  import profile.api._

  var db: Database = _

  def dbReady(ds: Seq[(DataType, Json, Json)] = Seq.empty) = {
    val is = ds.map {
      case (dt, v, r) =>
        sqlu"""insert into transformation_cache(data_type, value, replace)
               VALUES(${dt.toString()}, ${v.noSpaces}, ${r.noSpaces})"""
    }

    db.run(for {
      _ <- sqlu"#$createTransformationCacheSql"
      _ <- DBIO.sequence(is)
    } yield ())
  }

  before {
    db = Database.forURL(
      s"jdbc:h2:mem:${java.util.UUID.randomUUID.toString()};DATABASE_TO_LOWER=true;DB_CLOSE_DELAY=-1",
      driver = "org.h2.Driver"
    )
  }

  after {
    db.close()
  }

  test("read replace")(
    dbReady(Seq((DataType.FirstName, "Alexey".asJson, "Aleksei".asJson))).flatMap { _ =>
      val cache = DbCache(profile, db)
      cache.get(DataType.FirstName, "Alexey".asJson).map {
        _.value shouldBe "Aleksei".asJson
      }
    }
  )

  test("read all replaces")(
    dbReady(
      Seq(
        (DataType.FirstName, "Alexey".asJson, "Aleksei".asJson),
        (DataType.FirstName, "Valeriya".asJson, "Valeriia".asJson)
      )
    ).flatMap { _ =>
      val cache = DbCache(profile, db)
      cache.getAll(DataType.FirstName).map {
        _ should contain theSameElementsAs Seq(
          "Alexey".asJson -> "Aleksei".asJson,
          "Valeriya".asJson -> "Valeriia".asJson
        )
      }
    }
  )

  test("write replace")(
    dbReady().flatMap { _ =>
      val dt = DataType.Money
      val v = 100.0.asJson
      val r = 150.0.asJson

      val cache = DbCache(profile, db)

      for {
        _ <- cache.put(dt, v, r)
        got <- cache.get(dt, v)
      } yield {
        got.value shouldBe r
      }
    }
  )

  test("write all replace") {
    dbReady().flatMap { _ =>
      val dt = DataType.Date
      val vrs = Seq(
        "2010-10-25".asJson -> "2020-05-25".asJson,
        "1970-01-01".asJson -> "2000-10-01".asJson
      )

      val cache = DbCache(profile, db)

      for {
        _ <- cache.putAll(dt, vrs)
        got <- cache.getAll(dt)
      } yield {
        got should contain theSameElementsAs vrs
      }
    }
  }
}
