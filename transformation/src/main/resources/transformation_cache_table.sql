-- @block create TRANSFORMATION_CACHE table
drop table if exists TRANSFORMATION_CACHE ;

create table TRANSFORMATION_CACHE (
    DATA_TYPE VARCHAR not null,
    VALUE VARCHAR not null,
    REPLACE VARCHAR not null,
    PRIMARY KEY (DATA_TYPE, VALUE)
) ;