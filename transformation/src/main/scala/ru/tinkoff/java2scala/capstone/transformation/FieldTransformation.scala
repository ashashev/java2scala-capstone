package ru.tinkoff.java2scala.capstone.transformation

import scala.annotation.unused
import scala.concurrent.ExecutionContext
import scala.concurrent.Future

import io.circe.Json

trait FieldTransformation {
  def apply(v: Json): Future[Json]
}

object IdentityTransformation extends FieldTransformation {
  override def apply(v: Json): Future[Json] = Future.successful(v)
}

final class RandomTransformation(g: DataGenerator) extends FieldTransformation {
  override def apply(@unused v: Json): Future[Json] = Future.successful(g.gen())
}

object RandomTransformation {
  def apply(g: DataGenerator): RandomTransformation = new RandomTransformation(g)
}

final class RandomWithCacheTransformation(
    g: DataGenerator,
    dt: DataType,
    cache: Cache
  )(implicit ec: ExecutionContext)
    extends FieldTransformation {
  override def apply(v: Json): Future[Json] =
    cache.get(dt, v).flatMap {
      case Some(replace) => Future.successful(replace)
      case None =>
        val replace = g.gen()
        cache.put(dt, v, replace).map(_ => replace)
    }
}

object RandomWithCacheTransformation {
  def apply(
      g: DataGenerator,
      dt: DataType,
      cache: Cache
    )(implicit ec: ExecutionContext
    ): RandomWithCacheTransformation =
    new RandomWithCacheTransformation(g, dt, cache)
}
