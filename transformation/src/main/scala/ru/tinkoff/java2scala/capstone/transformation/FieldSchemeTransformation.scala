package ru.tinkoff.java2scala.capstone.transformation

import cats.syntax.either._
import cats.syntax.functor._
import io.circe.Codec
import io.circe.Decoder
import io.circe.DecodingFailure
import io.circe.Encoder
import io.circe.generic.semiauto._
import io.circe.syntax._

sealed trait FieldSchemeTransformation extends Product with Serializable

object FieldSchemeTransformation {
  implicit val encodeTransformation: Encoder[FieldSchemeTransformation] = Encoder.instance {
    case IdentityScheme => IdentityScheme.asJson
    case t: ReplaceScheme => t.asJson
  }

  implicit val decodeTransformation: Decoder[FieldSchemeTransformation] =
    List[Decoder[FieldSchemeTransformation]](
      Decoder[IdentityScheme.type].widen,
      Decoder[ReplaceScheme].widen
    ).reduceLeft(_ or _)
}

case object IdentityScheme extends FieldSchemeTransformation {

  private val INNER_NAME = "IDENTITY"

  implicit val encodeIdentity: Encoder[IdentityScheme.type] =
    Encoder.instance(_ => INNER_NAME.asJson)

  implicit val decodeIdentity: Decoder[IdentityScheme.type] = Decoder.instance { hcur =>
    hcur.as[String].flatMap {
      case INNER_NAME => IdentityScheme.asRight
      case s =>
        DecodingFailure(s"The '$s' is unknown type of the field transformation.", hcur.history).asLeft
    }
  }

}

final case class ReplaceScheme(`type`: DataType, useCache: Boolean = false)
    extends FieldSchemeTransformation

object ReplaceScheme {
  implicit val codecReplaceScheme: Codec[ReplaceScheme] = deriveCodec[ReplaceScheme]
}
