package ru.tinkoff.java2scala.capstone.transformation

import io.circe._
import io.circe.syntax._
import org.scalacheck.Gen
import org.scalacheck.rng.Seed
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import ru.tinkoff.java2scala.capstone.transformation.DataType._

trait DataGenerator {
  def gen(): Json
}

trait DataGeneratorFactory {
  def apply(dt: DataType): DataGenerator
}

class SimpleDataGeneratorFactory extends DataGeneratorFactory {
  def apply(dt: DataType): DataGenerator = dt match {
    case Address => DataGenerator.random(DataGenerator.address)
    case BankAccount => DataGenerator.random(DataGenerator.bankAccount)
    case Date => DataGenerator.random(DataGenerator.date)
    case Email => DataGenerator.random(DataGenerator.email)
    case FirstName => DataGenerator.random(DataGenerator.name)
    case LastName => DataGenerator.random(DataGenerator.name)
    case Money => DataGenerator.random(DataGenerator.money)
    case Phone => DataGenerator.random(DataGenerator.phone)
  }
}

object DataGenerator {

  def random(g: Gen[Json]): DataGenerator = () => g.pureApply(Gen.Parameters.default, Seed.random())

  private val properName: Gen[String] = for {
    first <- Gen.alphaUpperChar
    len <- Gen.chooseNum(1, 30)
    middle <- Gen.containerOfN[Seq, Char](len, Gen.alphaLowerChar).map(_.mkString)
  } yield s"$first$middle"

  private def alphaStrFixedLen(len: Int): Gen[String] =
    Gen.containerOfN[Seq, Char](len, Gen.alphaChar).map(_.mkString)

  private def alphaStrVariableLen(min: Int, max: Int): Gen[String] =
    for {
      len <- Gen.chooseNum(min, max)
      r <- alphaStrFixedLen(len)
    } yield r

  private def alphaNumStrFixedLen(len: Int): Gen[String] =
    Gen.containerOfN[Seq, Char](len, Gen.alphaNumChar).map(_.mkString)

  private def alphaNumStrVariableLen(min: Int, max: Int): Gen[String] =
    for {
      len <- Gen.chooseNum(min, max)
      r <- alphaNumStrFixedLen(len)
    } yield r

  val email: Gen[Json] = for {
    login <- alphaNumStrVariableLen(2, 20).map(_.toLowerCase())
    domain <- alphaNumStrVariableLen(2, 10).map(_.toLowerCase())
    zone <- alphaStrVariableLen(2, 5).map(_.toLowerCase())
  } yield s"$login@$domain.$zone".asJson

  val name: Gen[Json] = properName.map(_.asJson)

  val phone: Gen[Json] = for {
    f <- Gen.containerOfN[List, Char](10, Gen.numChar)
  } yield s"+7${f.mkString("")}".asJson

  val address: Gen[Json] =
    for {
      city <- properName
      sreet <- properName
      house <- Gen.chooseNum(1, 300)
      flat <- Gen.chooseNum(1, 5000)
    } yield s"$city, st. $sreet, $house, $flat".asJson

  val bankAccount: Gen[Json] = for {
    num <- Gen.containerOfN[List, Char](10, Gen.numChar).map(_.mkString(""))
    code <- Gen.containerOfN[List, Char](4, Gen.alphaUpperChar).map(_.mkString(""))
    ext <- Gen.containerOfN[List, Char](3, Gen.numChar).map(_.mkString(""))
  } yield s"$num-$code-$ext".asJson

  val money: Gen[Json] = for {
    m <- Gen.posNum[Int]
  } yield (BigDecimal(m) / 100.0).asJson

  val date: Gen[Json] = {
    val start = LocalDate.of(1980, 1, 1)
    val end = LocalDate.of(2050, 12, 31)
    val days = java.time.Duration
      .between(
        LocalDateTime.of(start, LocalTime.MIDNIGHT),
        LocalDateTime.of(end, LocalTime.MIDNIGHT)
      )
      .toDays
    for {
      d <- Gen.chooseNum(0, days)
    } yield start.plusDays(d).asJson
  }

}
