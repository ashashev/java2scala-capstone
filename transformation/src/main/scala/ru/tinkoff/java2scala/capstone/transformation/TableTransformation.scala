package ru.tinkoff.java2scala.capstone.transformation

import ru.tinkoff.java2scala.capstone.commons.FieldName
import ru.tinkoff.java2scala.capstone.commons.TableName

final case class TableTransformation(
    source: TableName,
    destination: TableName,
    fields: Map[FieldName, FieldTransformation])
