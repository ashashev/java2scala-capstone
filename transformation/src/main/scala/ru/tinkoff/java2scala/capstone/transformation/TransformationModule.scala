package ru.tinkoff.java2scala.capstone.transformation

import scala.annotation.nowarn

import com.softwaremill.macwire.Module
import com.softwaremill.macwire._

import ru.tinkoff.java2scala.capstone.commons.RuntimeModule

@Module @nowarn("msg=never used")
class TransformationModule(runtimeModule: RuntimeModule) {
  import runtimeModule._

  val cache: Cache = wire[DbCache]

  private val generators: DataGeneratorFactory = wire[SimpleDataGeneratorFactory]

  val fieldTransformationFactory: FieldTransformationFactory =
    wire[SimpleFieldTransformationFactory]
}
