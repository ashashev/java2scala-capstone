package ru.tinkoff.java2scala.capstone.transformation

import scala.concurrent.ExecutionContext
import scala.concurrent.Future

import akka.Done
import io.circe._
import slick.jdbc.JdbcProfile

trait Cache {
  def get(dt: DataType, v: Json): Future[Option[Json]]
  def getAll(dt: DataType): Future[Seq[(Json, Json)]]

  def put(dt: DataType, v: Json, r: Json): Future[Done]
  def putAll(dt: DataType, vrs: Seq[(Json, Json)]): Future[Done]
}

class DbCache(profile: JdbcProfile, db: JdbcProfile#API#Database)(implicit ec: ExecutionContext)
    extends Cache {
  import profile.api._

  implicit private val dataTypeMappedType = MappedColumnType.base[DataType, String](
    _.toString,
    DataType.withNameInsensitive(_)
  )

  implicit private val jsonMappedType = MappedColumnType.base[Json, String](
    _.noSpaces,
    s => parser.parse(s).toTry.get
  )

  private case class Info(dt: DataType, value: Json, replace: Json)

  private class CacheTable(tag: Tag) extends Table[Info](tag, "transformation_cache") {
    def dt = column[DataType]("data_type")
    def value = column[Json]("value")
    def replace = column[Json]("replace")

    override def * = (dt, value, replace) <> (Info.tupled, Info.unapply)
  }

  private val cache = TableQuery[CacheTable]

  override def get(dt: DataType, value: Json): Future[Option[Json]] =
    for {
      rows <- db.run(cache.filter(t => t.dt === dt && t.value === value).map(_.replace).result)
    } yield rows.headOption

  override def getAll(dt: DataType): Future[Seq[(Json, Json)]] =
    for {
      rows <- db.run(cache.filter(t => t.dt === dt).map(r => r.value -> r.replace).result)
    } yield rows

  override def put(dt: DataType, value: Json, replace: Json): Future[Done] =
    for {
      _ <- db.run(cache += Info(dt, value, replace))
    } yield Done

  override def putAll(dt: DataType, vrs: Seq[(Json, Json)]): Future[Done] =
    for {
      _ <- db.run(cache ++= vrs.map { case (value, replace) => Info(dt, value, replace) })
    } yield Done

}

object DbCache {
  def apply(profile: JdbcProfile, db: JdbcProfile#API#Database)(implicit ec: ExecutionContext) =
    new DbCache(profile, db)
}
