package ru.tinkoff.java2scala.capstone.transformation

import enumeratum._

sealed trait DataType extends EnumEntry

object DataType extends Enum[DataType] with CirceEnum[DataType] {
  val values = findValues

  case object FirstName extends DataType
  case object LastName extends DataType
  case object Phone extends DataType
  case object Email extends DataType
  case object Address extends DataType
  case object BankAccount extends DataType
  case object Money extends DataType
  case object Date extends DataType
}
