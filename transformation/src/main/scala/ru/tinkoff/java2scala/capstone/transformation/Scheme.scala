package ru.tinkoff.java2scala.capstone.transformation

import io.circe._
import io.circe.generic.semiauto._

import ru.tinkoff.java2scala.capstone.commons.FieldName
import ru.tinkoff.java2scala.capstone.commons.TableName

final case class Scheme(
    tableFrom: TableName,
    tableTo: TableName,
    transformations: Map[FieldName, FieldSchemeTransformation])

object Scheme {
  implicit val codecScheme: Codec[Scheme] = deriveCodec[Scheme]
}
