package ru.tinkoff.java2scala.capstone.transformation

import scala.concurrent.ExecutionContext

trait FieldTransformationFactory {
  def apply(scheme: FieldSchemeTransformation): FieldTransformation
}

class SimpleFieldTransformationFactory(
    cache: Cache,
    gens: DataGeneratorFactory
  )(implicit ec: ExecutionContext)
    extends FieldTransformationFactory {
  def apply(scheme: FieldSchemeTransformation): FieldTransformation = scheme match {
    case IdentityScheme => IdentityTransformation
    case ReplaceScheme(dt, false) => RandomTransformation(gens(dt))
    case ReplaceScheme(dt, true) => RandomWithCacheTransformation(gens(dt), dt, cache)
  }
}
