package ru.tinkoff.java2scala.capstone.transformation

import scala.concurrent.Future
import scala.concurrent.ExecutionContext
import scala.util.control.NoStackTrace

import akka.NotUsed
import akka.stream.scaladsl.Flow
import io.circe._
import io.circe.syntax._

import ru.tinkoff.java2scala.capstone.commons.FieldName
import ru.tinkoff.java2scala.capstone.utils.json.matchers.IsObject

object Transformation {
  final case class NoFieldTransformation(field: FieldName)
      extends RuntimeException(s"No transformation for the '${field.get}' field.")
      with NoStackTrace

  def flow(
      t: Map[FieldName, FieldTransformation],
      parallelism: Int = 1
    )(implicit ec: ExecutionContext
    ): Flow[Json, Json, NotUsed] =
    Flow[Json]
      .collect {
        case IsObject(o) => o
      }
      .mapAsync(parallelism) { o =>
        Future
          .traverse(o.toList) {
            case (fieldName, value) if t.contains(FieldName(fieldName)) =>
              t(FieldName(fieldName))(value).map(fieldName -> _)
            case (fieldName, _) =>
              Future.failed(NoFieldTransformation(FieldName(fieldName)))
          }
          .map(fs => JsonObject(fs: _*).asJson)
      }

}
