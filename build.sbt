import Dependencies._
import CompilerOptions._

ThisBuild / scalaVersion := "2.13.2"
ThisBuild / version := "0.1.0-SNAPSHOT"
ThisBuild / name := "java2scala-capstone"
ThisBuild / fork := true

lazy val root = (project in file("."))
  .aggregate(
    capstone,
    db,
    manager,
    transformation,
    utils
  )

lazy val utils = module("utils")

lazy val commons = module("commons")
  .settings(
    libraryDependencies ++= Seq(
      slick
    )
  )

lazy val db = module("db")
  .settings(
    libraryDependencies ++= Seq(
      slick,
      slickHikaricp,
      slickH2db % Test,
      postgres % Test
    )
  )
  .dependsOn(commons, utils)

lazy val transformation = module("transformation")
  .settings(
    libraryDependencies ++= Seq(
      scalaCheck,
      slick,
      slickHikaricp % Test,
      slickH2db % Test
    )
  )
  .dependsOn(commons, utils)

lazy val manager = module("manager")
  .settings(
    libraryDependencies ++= akkaHttp,
    libraryDependencies ++= Seq(
      slickH2db,
      postgres
    )
  )
  .dependsOn(`commons`, `db`, `transformation`, `utils`)

lazy val cachecontrol = module("cachecontrol")
  .settings(
    libraryDependencies ++= akkaHttp,
    libraryDependencies ++= Seq(
      slickH2db,
      postgres
    )
  )
  .dependsOn(`commons`, `transformation`, `utils`)

lazy val capstone = module("capstone")
  .enablePlugins(JavaAppPackaging)
  .settings(
    libraryDependencies ++= Seq(
      slick,
      slickHikaricp,
      slickH2db,
      postgres
    ),
    dockerExposedPorts ++= Seq(3435)
  )
  .dependsOn(cachecontrol, manager)

def module(name: String): Project =
  Project(name, file(name))
    .settings(
      commonSettings
    )

lazy val commonSettings = Seq(
  libraryDependencies ++= Seq(
    scalaTest % Test,
    scalaTestPlusScalaCheck % Test,
    scalaMock % Test,
    enumeratum,
    enumeratumCirce
  ),
  libraryDependencies ++= circe,
  libraryDependencies ++= cats,
  libraryDependencies ++= akkaStream,
  libraryDependencies ++= scalaLogging,
  libraryDependencies ++= macwire,
  scalacOptions ++= compilerOptions,
  fork := true
)
