
-- @block fill test data

insert into person(id, firstname, lastname, phone, email) values (
  'p-1',
  'Alexey',
  'Shashev',
  '+75121234567',
  'ash@nothing.none'
);

insert into person(id, firstname, lastname, phone, email) values (
  'p-2',
  'Natasha',
  'Volkova',
  '+77278765432',
  'nusha@nothing.none'
);

insert into account(id, person_id, number, balance, open, status) values (
  'a-1',
  'p-1',
  '1234000012-AAAA-001',
  0.0,
  '2014-09-13',
  'close'
) ;

insert into account(id, person_id, number, balance, open, status) values (
  'a-2',
  'p-2',
  '6532000023-AAAB-123',
  542000.0,
  '2015-03-15',
  'open'
) ;

insert into account(id, person_id, number, balance, open, status) values (
  'a-3',
  'p-1',
  '5423000054-BAAC-023',
  100000.0,
  '2018-03-15',
  'open'
) ;

insert into transaction(id, account_id, sum, date, description) values (
  't-1',
  'a-1',
  25000.0,
  '2014-09-25',
  'income'
) ;

insert into transaction(id, account_id, sum, date, description) values (
  't-2',
  'a-1',
  5000.0,
  '2014-09-28',
  'outcome'
) ;

insert into transaction(id, account_id, sum, date, description) values (
  't-3',
  'a-1',
  20000.0,
  '2014-10-01',
  'outcome'
) ;

insert into marketing_newsletter(id, email, send_status, status) values (
  'mn-1',
  'ash@nothing.none',
  'NOT_SEND',
  true
) ;

insert into marketing_newsletter(id, email, send_status, status) values (
  'mn-1',
  'nusha@nothing.none',
  'SENT',
  true
) ;

insert into marketing_newsletter(id, email, send_status, status) values (
  'mn-2',
  'ash@nothing.none',
  'NOT_SEND',
  false
) ;

insert into marketing_newsletter(id, email, send_status, status) values (
  'mn-2',
  'nusha@nothing.none',
  'NOT_SEND',
  false
) ;