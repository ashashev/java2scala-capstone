-- @block create tables
drop table if exists person;

create table person (
  id varchar primary key,
  firstname varchar not null,
  lastname varchar not null,
  phone varchar not null,
  email varchar not null
);

drop table if exists account ;

create table account (
  id varchar primary key,
  person_id varchar not null,
  number varchar not null,
  balance decimal not null default 0.0,
  open date not null,
  status varchar not null
);

drop table if exists marketing_newsletter ;

create table marketing_newsletter (
  id varchar not null,
  email varchar not null,
  send_status varchar not null,
  status boolean,
  primary key (id, email)
);

drop table if exists transaction ;

create table transaction (
  id varchar primary key,
  account_id varchar not null,
  sum decimal not null,
  date date,
  description varchar
);
